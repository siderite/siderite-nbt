# siderite_nbt

This is a Rust library for working with Minecraft's Named Binary Tag (NBT) file format. It is forked from [hematite_nbt] maintained by the [Siderite] project.

[hematite_nbt]: https://github.com/PistonDevelopers/hematite_nbt
[Siderite]: https://gitlab.com/siderite
